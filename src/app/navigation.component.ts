
import {Component, OnInit, Input, Output, EventEmitter} from "@angular/core";

import {NavigationNode} from "./navigation-node";
import {NavigationOption} from "./navigation-option";

@Component({
  selector: 'nav',
  templateUrl: 'navigation.component.html'
})
export class NavigationComponent implements OnInit {

    @Input() public navNode : NavigationNode;
    @Output() onNewState = new EventEmitter<string>();

    ngOnInit() : void {
    }

    onSelect (option : NavigationOption) : void{
      this.onNewState.emit(option.nextState);
    }
}
