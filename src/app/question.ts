
export class Question {
  public text : string;
  public moreInfo?: string;
  public answer? : number;
  public scores? : number [];

}
