
import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import { Information } from './information';

@Component({
  selector: 'info',
  templateUrl: 'information.component.html'
})
export class InformationComponent implements OnInit {

  @Input() public information: Information;
  @Output() onGoBack = new EventEmitter();

    ngOnInit(): void {
    }

    goBack(): void {
      this.onGoBack.emit();
    }
}
