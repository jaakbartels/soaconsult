export class Screening {
    public name: string;
    public scoreIndex: number;
    public info?: string;
    public infoVisible? : boolean;
}
