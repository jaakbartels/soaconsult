import {NavigationNode } from './navigation-node'
import {Questionnaire } from './questionnaire'
import {Information} from './information'

export class State {
  public title? : string;
  public subtitle?: string;
  public navigationNode? : NavigationNode;
  public questionnaire? : Questionnaire;
  public results? : boolean;
  public information? : Information;
}
