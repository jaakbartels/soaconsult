import { SCREENINGS } from './screenings';
import { Screening } from './screening';
import { STATES } from './states';
import { State } from './state';

export class Results {

    private totalScores : number[];

    public init(visitedStates: string[]) : void{
        this.totalScores = Array<number>(SCREENINGS.length).fill(0);

        visitedStates.forEach( stateCode => {
            let state : State = STATES[stateCode];
            if (state.questionnaire){
                state.questionnaire.questions.forEach(q => {
                    if (q.answer == 1 ) {
                        for (var i=0; i<this.totalScores.length; i++){
                            this.totalScores[i] += q.scores[i];
                        }
                    }
                });
            }
        });
    }

    public screenings() : Screening[] {
        return SCREENINGS.filter((s, idx)=> this.totalScores[s.scoreIndex] >= 1);
    }
}
