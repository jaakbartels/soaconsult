import { NavigationOption} from './navigation-option'


export class NavigationNode {
    public question: string;
    public options: NavigationOption[];
}

