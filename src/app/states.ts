import {State} from './state';

export const STATES: {[code: string]: State} = {
  'start': {
    title: 'Type consult :',
    navigationNode: {
      question: '',
      options: [{
        answer: 'SOA consult',
        nextState: 'gender'
      },
        {
          answer: 'Occasioneel consult',
          nextState: 'quest4'
        }]
    }
  },
  'gender': {
    navigationNode: {
      question: 'De patiënt is een ...',
      options: [{
        answer: 'man',
        nextState: 'complaints_m'
      },
        {
          answer: 'vrouw',
          nextState: 'complaints_f'
        }]
    }
  },
  'complaints_m': {
    navigationNode: {
      question: 'Heeft de patiënt klachten ?',
      options: [{
        answer: 'Ja',
        nextState: 'quest2_m'
      },
        {
          answer: 'Nee',
          nextState: 'quest1_m'
        }]
    }
  },
  'complaints_f': {
    navigationNode: {
      question: 'Heeft de patiënt klachten ?',
      options: [{
        answer: 'Ja',
        nextState: 'quest2_f'
      },
        {
          answer: 'Nee',
          nextState: 'quest1_f'
        }]
    }
  },
  'quest1_m': {
    title: 'Risicofactoren bij de man :',
    subtitle: 'Deze vragenlijst kan u als arts samen invullen met uw patiënt tijdens de anamnese',
    questionnaire: {
      questions: [
        {
          text: 'Heeft uw patiënt het laatste jaar wisselende heteroseksuele contacten gehad? ',
          scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        },
        {
          text: 'Heeft uw patiënt het laatste jaar onveilig seksueel contact gehad? ',
          scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        },
        {
          text: 'Heeft uw patiënt het laatste jaar onveilige seksueel contact gehad met meerdere partners? ',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {text: 'Is uw patiënt ongerust dat hij een soa heeft? ', scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0]},
        {text: 'Heeft uw patiënt een partner gehad die besmet was met een soa? ', scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]},
        {text: 'Heeft uw patiënt homo- of biseksuele contacten gehad? ', scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]},
        {
          text: 'Heeft uw patiënt een partner gehad die homo- of biseksuele contacten heeft gehad?',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Heeft uw patiënt of één van zijn partners in de prostitutie gewerkt? ',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Is uw patiënt of één van zijn partners afkomstig uit een HIV-endemisch gebied?',
          moreInfo: 'hiv-areas',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Gebruikt uw patiënt of één van zijn partners drug via intraveneuze toedieningsweg? ',
          scores: [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Is uw patiënt of één van zijn partners afkomstig uit een Hepatitis B-endemisch gebied?',
          moreInfo: 'hepatitis-b-areas',
          scores: [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
        },
      ],
      nextState: 'result'
    }
  },
  'quest1_f': {
    title: 'Risicofactoren bij de vrouw :',
    subtitle: 'Deze vragenlijst kan u als arts samen invullen met uw patiënte tijdens de anamnese',
    questionnaire: {
      questions: [
        {
          text: 'Heeft uw patiënte het laatste jaar wisselende heteroseksuele contacten gehad? ',
          scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        },
        {
          text: 'Heeft uw patiënte het laatste jaar onveilig seksueel contact gehad? ',
          scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        },
        {
          text: 'Heeft uw patiënte het laatste jaar onveilige seksueel contact gehad met meerdere partners? ',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {text: 'Is uw patiënte ongerust dat zij een soa heeft? ', scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0]},
        {text: 'Heeft uw patiënte een partner gehad die besmet was met een soa? ', scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]},
        {text: 'Heeft uw patiënte homo- of biseksuele contacten gehad? ', scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]},
        {
          text: 'Heeft uw patiënte een partner gehad die homo- of biseksuele contacten heeft gehad?',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Heeft uw patiënte of één van haar partners in de prostitutie gewerkt? ',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Is uw patiënte of één van haar partners afkomstig uit een HIV-endemisch gebied?',
          moreInfo: 'hiv-areas',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Gebruikt uw patiënte of één van haar partners drug via intraveneuze toedieningsweg? ',
          scores: [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Is uw patiënte of één van haar partners afkomstig uit een Hepatitis B-endemisch gebied?',
          moreInfo: 'hepatitis-b-areas',
          scores: [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
        },
      ],      
      nextState: 'quest4'
    }
  },
  'quest2_m': {
    title: 'Risicofactoren in aanwezigheid van klachten bij de man :',
    subtitle: 'Deze vragenlijst kan u als arts samen invullen met uw patiënt tijdens de anamnese',
    questionnaire: {
      questions: [
        {
          text: 'Heeft uw patiënt of één van zijn partners als man seks gehad met een andere man? ',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Heeft uw patiënt of één van zijn partners de laatste 6 maanden in de prostitutie gewerkt? ',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Is uw patiënt of één van zijn partners afkomstig uit een soa-endemisch gebied?',
          moreInfo: 'soa-areas',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Heeft uw patiënt of één van zijn partners 3 of meer seksuele partners gehad de laatste 6 maanden? ',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
      ],
      nextState: 'quest3_m'
    }
  },
  'quest2_f': {
    title: 'Risicofactoren in aanwezigheid van klachten bij de vrouw  :',
    subtitle: 'Deze vragenlijst kan u als arts samen invullen met uw patiënte tijdens de anamnese',
    questionnaire: {
      questions: [
        {
          text: 'Heeft uw patiënte of één van haar partners als man seks gehad met een andere man? ',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Heeft uw patiënte of één van haar partners de laatste 6 maanden in de prostitutie gewerkt? ',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Is uw patiënte of één van haar partners afkomstig uit een soa-endemisch gebied?',
          moreInfo: 'soa-areas',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
        {
          text: 'Heeft uw patiënte of één van haar partners 3 of meer seksuele partners gehad de laatste 6 maanden? ',
          scores: [1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        },
      ],
      nextState: 'quest3_f'
    }
  },
  'quest3_m': {
    title: 'Klachten bij de man :',
    subtitle: 'Volgende vragenlijst dient u als arts zelf in te vullen op basis van uw interpretatie van de anamnese en het klinisch onderzoek',
    questionnaire: {
      questions: [
        {text: 'Urethritis met afscheiding? ', scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Afscheiding uit urethra of anus?', scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Pijn of branderig gevoel bij het plassen?', scores: [1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]},
        {text: 'Genitale jeuk of irritatie?', scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Pijn of zwelling van de bijbal?', scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Roodheid of zwelling van het scrotum? ', scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Urethritis zonder afscheiding? ', scores: [1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]},
        {text: 'Klachten van epididymitis?', scores: [1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]},
        {text: 'Een hard en pijnloos ulcus?', scores: [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]},
        {text: 'Genitale wratten?', scores: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Vesikeltjes?', scores: [1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0]},
        {text: 'Proctitis klachten?', scores: [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Anale seksuele contacten de laatste 6 maanden?', scores: [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Anale ulcera of erosies?', scores: [0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0]},
        {text: 'Orale klachten?', scores: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]},
      ],
      nextState: 'result'
    }
  },
  'quest3_f': {
    title: 'Klachten bij de vrouw :',
    subtitle: 'Volgende vragenlijst dient u als arts zelf in te vullen op basis van uw interpretatie van de anamnese en het klinisch onderzoek',
    questionnaire: {
      questions: [
        {text: 'Afscheiding uit urethra of anus?', scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Pijn of branderig gevoel bij het plassen?', scores: [1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]},
        {text: 'Genitale jeuk of irritatie?', scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {
          text: 'Afscheiding, contactbloeding, intermenstrueel bloedverlies of onderbuikspiijn bij de vrouw?',
          scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        },
        {text: 'Vaginale fluor bij de vrouw?', scores: [1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0]},
        {text: 'Een hard en pijnloos ulcus?', scores: [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]},
        {text: 'Genitale wratten?', scores: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Vesikeltjes?', scores: [1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0]},
        {text: 'Proctitis klachten?', scores: [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Anale seksuele contacten de laatste 6 maanden?', scores: [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Anale ulcera of erosies?', scores: [0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0]},
        {text: 'Orale klachten?', scores: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]},
      ],
      nextState: 'result'
    }
  },
  'quest4': {
    title: 'Risicobepaling :',
    subtitle: 'Deze vragenlijst kan u als arts samen invullen met uw patiënt',
    questionnaire: {
      questions: [
        {text: 'Is uw patiënte een vrouw jonger dan 35 jaar?', scores: [0.4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Heeft uw patiënte meer dan 1 seksuele partner gehad? ', scores: [0.4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {text: 'Heeft uw patiënte de laatste 6 maand een nieuwe partner gehad? ', scores: [0.4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
      ],
      nextState: 'result'
    }
  },
  'result': {
    title: 'Aanbevolen screenings :',
    results: true
  },
  'hiv-areas': {
    title: "HIV-endemische gebieden",
    information: {
      preImageText: '<ol><li>Afrika, ten zuiden van de Sahara</li><li>Caraïbisch gebied</li></ol>',
      imageUrl: 'assets/hiv_areas.jpg',
      postImageText: '<p>Bij twijfel is het aangeraden om laagdrempelig te screenen en deze vraag dus met ja te beantwoorden.<br/>Voor een volledige landenlijst kan u terecht op de website van <a target="_blank" href="https://www.rivm.nl/Documenten_en_publicaties/Professioneel_Praktisch/Richtlijnen/Infectieziekten/Soa/Documenten_ASG/Download/Lijst_soa_hiv_endemische_landen">Rijksinstituut Volksgezondheid en Milieu</a>.</p>'
    }
  },
  'hepatitis-b-areas': {
    title: "Hepatitis B-endemische gebieden",
    information: {
      preImageText: '<ol><li>Zuid-oost Azië</li><li>China</li><li>Oceanië</li><li>Japan</li><li>Afrika, ten zuiden van de Sahara</li></ol>',
      imageUrl: 'assets/hepatitis_b_areas.jpg',
      postImageText: '<p>Bij twijfel is het aangeraden om laagdrempelig te screenen en deze vraag dus met ja te beantwoorden.</p>'
    }
  } ,
  'soa-areas': {
    title: "SOA-endemische gebieden",
    information: {
      preImageText: '<ol><li>Afrika</li><li>Marokko</li><li>Voormalige Nederlandse Antillen</li><li>Oost-Europa</li><li>Suriname</li><li>Turkije</li><li>Zuid-Amerika</li></ol>',
      imageUrl: 'assets/soa_areas.jpg',
      postImageText: '<p>Bij twijfel is het aangeraden om laagdrempelig te screenen en deze vraag dus met ja te beantwoorden.</p>'
    }
  }
};

