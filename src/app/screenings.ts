import {Screening} from './screening'

export const SCREENINGS: Screening[] = [
  {name: "Chlamydia", scoreIndex: 0, info: "PCR: op eerste straal urine, liefst ook op vaginaal secreet bij vrouwen"},
  {name: "Gonorroe", scoreIndex: 1, info: "PCR: op eerste straal urine, liefst ook op vaginaal secreet bij vrouwen, ook op keelwisser bij klachten in de keel"},
  {name: "Chlamydia PCR op rectale wisser", scoreIndex: 2, info: "PCR op rectale wisser"},
  {name: "Gonorroe PCR op rectale wisser", scoreIndex: 3, info: "PCR op rectale wisser"},
  {name: "Gonorroe op keelwisser", scoreIndex: 11, info: "PCR op keelwisser"},
  {name: "HIV", scoreIndex: 4, info: "Serologietesten: Bepaal anti-hiv1- en anti-hiv2-antilichamen"},
  {name: "Syfilis", scoreIndex: 5, info: "Serologietesten: TPHA en TPPA<br>Bij ulcus: PCR van ulcusmateriaal, verwijzing naar de dermatoloog<br>Indien negatieve serologie bij een ulcus kunnen de serologietesten best worden herhaald na enkele weken"},
  {name: "Hepatitis B", scoreIndex: 6, info: "Serologietesten: Bepaal HBsAg (indien niet gevaccineerd)"},
  {name: "Urinecultuur", scoreIndex: 7, info: "Urine dipstick, cultuur, antibiogram"},
  {name: "Herpes", scoreIndex: 8, info: "Wanneer het beeld klinisch onduidelijk is: swab af voor een amplificatietest of kweek.<br>PCR op ulcusmateriaal"},
  {name: "Trichomonas", scoreIndex: 9, info: "vrouwen: Vaginale swab voor PCR en specifieke trichomonaskweek of voor rechtstreeks microscopisch onderzoek<br> mannen: PCR van de urine, urethra-uitstrijkje waarop PCR of specifieke trichomonaskweek"},
  {name: "Fluor-Consult", scoreIndex: 10, info: "lichamelijk onderzoek: inspectie vulva, speculumonderzoek, vaginaal toucher en buikonderzoek<br> Onderzoek van fluor: pH van de fluor, amine test, microscopisch onderzoek, gistkweek, PCR chlamydia, PCR Gonnoroe en PCR of specifieke kweek voor trichomonas"},
];
