import { Question } from './question'

export class Questionnaire{
  public questions : Question [];
  public nextState? : string;
}
