import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Questionnaire} from "./questionnaire";
import {Question} from "./question";

@Component({
  selector: 'questionnaire',
  templateUrl: 'questionnaire.component.html'
})
export class QuestionnaireComponent {
  @Input() questionnaire: Questionnaire;
  @Output() onNewState = new EventEmitter<string>();

  public answered (question: Question, answer: number) : void {
    question.answer = answer;
  }

  public onContinue() : void {
    this.onNewState.emit(this.questionnaire.nextState);
  }

  public onRequestInfo(key: string): void {
    this.onNewState.emit(key);
  }
}
