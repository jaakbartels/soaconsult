import {Component} from '@angular/core';
import {State} from './state';
import {STATES} from './states';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  title = 'SOA consult';
  private states = STATES;
  state: State = this.states['start'];
  breadcrumbs: string[] = ['start'];

  public setNewState(newStateCode: string): void {
    this.breadcrumbs.push(newStateCode);
    this.state = this.states[newStateCode];
  }

  public goBack(): void {
    if (this.breadcrumbs.length > 1) {
      this.breadcrumbs.pop();
      this.state = this.states[this.breadcrumbs[this.breadcrumbs.length - 1]];
    }
  }

  public restart(): void {
    this.state = this.states['start'];
    this.breadcrumbs = ['start'];
    this.resetAllAnswers();
  }

  public canGoBack(): boolean {
    return this.breadcrumbs.length > 1;
  }

  public canRestart(): boolean {
    return this.state !== this.states['start'];
  }

  private resetAllAnswers(): void {
    for (const key in this.states) {
      let state = this.states[key];
      if (state.questionnaire) {
        for (let q of state.questionnaire.questions) {
          q.answer = 0;
        }
      }
    }
  }
}
