import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {Routes, RouterModule} from '@angular/router';

import { AppComponent }  from './app.component';
import {NavigationComponent} from "./navigation.component";
import {QuestionnaireComponent} from "./questionnaire.component";
import {ResultComponent} from './result.component';
import { InformationComponent } from './information.component';

// const appRoutes: Routes = [
//   { path: 'patient', component: AppComponent },
//   { path: 'arts', component: AppComponent },
//   { path: '',
//     redirectTo: '/patient',
//     pathMatch: 'full'
//   },
// ];

@NgModule({
  imports:
    [ 
      // RouterModule.forRoot(appRoutes),
      BrowserModule
    ],
  declarations:
    [ AppComponent,
      NavigationComponent,
      QuestionnaireComponent,
      InformationComponent,
      ResultComponent
    ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
