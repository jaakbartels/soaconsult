import {Component, OnInit, Input} from '@angular/core';
import {Screening} from './screening';
import {Results} from './results';

@Component({
    selector : 'result',
    templateUrl: 'result.component.html'
})
export class ResultComponent implements OnInit {

    @Input() visitedStates: string[];
    public results : Results;

    ngOnInit():void{
        this.results = new Results();
        this.results.init(this.visitedStates);
    }

    public toggleInfo(s : Screening):void{
       s.infoVisible = !s.infoVisible;
    }
}
